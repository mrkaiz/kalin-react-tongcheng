import axios from 'axios';
import md5 from 'md5';
import { Toast } from 'antd-mobile';

let user = {
    uid: '',
    token: '',
};

export function getUser() {
    return user;
}
export function setUser(uid, token) {
    user = {
        uid: uid,
        token: token,
    };
}
/** 
 * @author 所有方法共Ajax 
 * @param url:url链接地址 
 * @param params:参数 
 * @param type: 类型
 */
function BaseAjax(url, params, type = "GET") {
    let time = Math.round(new Date().getTime() / 1000);
    let token = user.token;
    let uid = user.uid || 0;
    let _url = 'http://anliantu.com/' + url;
    params = {
        ...params,
        time: time,
        uid: uid,
        token: md5(time + token + uid),
    }
    // console.log(params)
    if (type === "POST") {
        return new Promise((resolve, reject) => {
            axios.post(_url, {
                data: params
            }).then(function (response) {
                resolve(response.data)
            }).catch(function (err) {
                reject(err)
            })
        })
    } else {
        return new Promise((resolve, reject) => {
            axios.get(_url, {
                params: params
            }).then(function (response) {
                resolve(response.data)
            }).catch(function (err) {
                reject(err)
            })
        })
    }
}
/** 
 * @author 图片上传方法 
 * @param {base64} base64图片数据 
 */
export function UpImg(base64) {
    let _url = "http://anliantu.com/index/tool/upload";
    return new Promise((resolve, reject) => {
        Toast.loading("正在上传", 0);
        axios.post(_url, {
            data: {
                base64: base64,
            }
        }).then(function (response) {
            resolve(response.data)
            Toast.hide();
        }).catch(function (err) {
            reject(err)
        })
    })
}
/** 
 * @author 获取用户信息 
 */
export function getUserInfo() {
    var params;
    var url = "index/user/getinfo/";
    return new Promise((resolve, reject) => {
        BaseAjax(url, params).then(function (response) {
            resolve(response)
        }).catch(function (err) {
            reject(err)
        });
    })
}
/** 
 * @author 获取拼车列表
 */
export function getTravelList(params) {
    var url = "index/travel/query";
    return new Promise((resolve, reject) => {
        Toast.loading("加载中...", 3);
        BaseAjax(url, params).then(function (response) {
            Toast.hide();
            resolve(response)
        }).catch(function (err) {
            Toast.hide();
            reject(err)
        });

    })
}
/** 
 *@method 获取拼车详细
 *@param {id} 
 */
export function getTravelDetailed(id) {
    var url = "index/travel/detailed";
    var params = { id: id }
    return new Promise((resolve, reject) => {
        Toast.loading("加载中", 0);
        BaseAjax(url, params).then(function (response) {
            Toast.hide();
            resolve(response)
        }).catch(function (err) {
            Toast.hide();
            reject(err)
        });
    })
}
/** 
 *@method 添加车讯
 *@param {data} 
 */
export function addTravel(data) {
    var url = "index/travel/add";
    var params = data
    return new Promise((resolve, reject) => {
        Toast.loading("加载中", 0);
        BaseAjax(url, params, "POST").then(function (response) {
            Toast.hide();
            resolve(response)
        }).catch(function (err) {
            Toast.hide();
            reject(err)
        });
    })
}

/** 
 *@method 跳转支付页面
 *@param {act_id} 
 */
export function ActivityPay(act_id) {
    window.location.href = "http://anliantu.com/index/pay/index?uid=" + user.uid + "&act_id=" + act_id;
}

/** 
 *@method 统计详细
 *@param {id} 
 */
export function StatisticsData(id) {
    var url = "index/advert/statistics";
    var params = { act_id: id }
    return new Promise((resolve, reject) => {
        Toast.loading("加载中", 0);
        BaseAjax(url, params).then(function (response) {
            Toast.hide();
            resolve(response)
        }).catch(function (err) {
            Toast.hide();
            reject(err)
        });
    })
}