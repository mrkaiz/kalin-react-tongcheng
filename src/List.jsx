import React, { Component } from 'react';
import { Slider, Search, Chexun } from './component/component.jsx';
import { TabBar, NoticeBar } from 'antd-mobile';
import { getTravelList } from "./api";
import "./css/list.css";

let scrollTop = 0;
let data;
export default class List extends Component {

    constructor() {
        super();
        this.state = {
            getData: true,
            address: [],
            page: 1,
        }
        this.getList = this.getList.bind(this);
        this.search = this.search.bind(this);
    }
    componentDidMount() {
        scrollTop = window.scrollY;
        window.addEventListener('scroll', this.scrollHandler);
        var reload = false;
        if (!data || reload) {
            this.getList();
        } else {
            this.setState(data);
        }
    }
    getList() {
        var params = {
            start: this.state.page,
            address_start: this.state.address_start,
            address_end: this.state.address_end,
        };
        getTravelList(params).then(res => {
            if (res.data.length > 0) {
                this.setState({
                    address: [...this.state.address, ...res.data],
                    getData: true,
                    page: this.state.page + 1
                })
            }
        })
    }
    search(address_start, address_end) {
        this.setState({
            page: 1,
            address: [],
            getData: true,
            address_start: address_start,
            address_end: address_end,
        }, this.getList)
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.scrollHandler);
        window.scrollTo(0, scrollTop)
        data = this.state;
    }
    scrollHandler = (event) => {
        let scrollTop = event.srcElement.body.scrollTop;
        let listHeight = this.refs.list.scrollHeight;
        let windowHeight = window.screen.height;
        if (this.state.getData) {
            if (scrollTop + 100 > listHeight - windowHeight) {
                this.setState({ getData: false })
                this.getList();

            }
        }

    }
    render() {
        var imgs = [
            { img: require('./image/banner.png'), color: "#ed7152" },
        ];
        var { address } = this.state;

        return (
            <div className="list" ref="list">
                <Slider autoPlayInterval={3000} imgs={imgs} height="130px" />
                <NoticeBar marqueeProps={{ loop: true, style: { padding: '0 7.5px' } }}>
                    免责声明：本平台不对任何人提供任何形式的担保 ，所有信息仅供参考，不承担由此产生的任何民事及法律 责任。
                </NoticeBar>
                <Search search={this.search} />
                <div className="chexunlist" style={{ marginBottom: '55px' }}>
                    {
                        address.map(function (item, i) {
                            return (<Chexun key={i} data={item} />)
                        })
                    }

                </div>
                <TabBar
                    unselectedTintColor="#666666"
                    tintColor="#00a8ec"
                >
                    <TabBar.Item
                        title="列表"
                        key="列表"
                        icon={<div style={{
                            width: '22px',
                            height: '22px',
                            background: 'url(' + require('./image/icon_cart.png') + ') center center /  21px 21px no-repeat'
                        }}
                        />
                        }
                        selectedIcon={<div style={{
                            width: '22px',
                            height: '22px',
                            background: 'url(' + require('./image/icon_cart_on.png') + ') center center /  21px 21px no-repeat'
                        }}
                        />
                        }
                        selected={true}
                    />
                    <TabBar.Item
                        title="资讯"
                        key="资讯"
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' + require('./image/icon_cattle.png') + ') center center /  21px 21px no-repeat'
                            }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' + require('./image/icon_cattle_on.png') + ') center center /  21px 21px no-repeat'
                            }}
                            />
                        }
                        selected={false}
                        onPress={() => { this.props.history.push('/cattle'); }}
                    />
                    <TabBar.Item
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' + require('./image/icon_put.png') + ') center center /  21px 21px no-repeat'
                            }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' + require('./image/icon_put_on.png') + ') center center /  21px 21px no-repeat'
                            }}
                            />
                        }
                        title="发布"
                        key="发布"
                        selected={false}
                        onPress={() => { this.props.history.push('/edit'); }}
                    />
                </TabBar>
            </div>
        )
    }
}