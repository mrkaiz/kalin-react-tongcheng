import React, { Component } from 'react';
import { TabBar } from 'antd-mobile';
import { News } from './component/component.jsx';
export default class Cattle extends Component {
    render() {
        return (
            <div className="cattle">
                <News />
                <News />
                <TabBar
                    unselectedTintColor="#666666"
                    tintColor="#00a8ec"
                >
                    <TabBar.Item
                        title="列表"
                        key="列表"
                        icon={<div style={{
                            width: '22px',
                            height: '22px',
                            background: 'url(' + require('./image/icon_cart.png') + ') center center /  21px 21px no-repeat'
                        }}
                        />
                        }
                        selectedIcon={<div style={{
                            width: '22px',
                            height: '22px',
                            background: 'url(' + require('./image/icon_cart_on.png') + ') center center /  21px 21px no-repeat'
                        }}
                        />
                        }
                        onPress={() => { this.props.history.push('/list'); }}
                    />
                    <TabBar.Item
                        title="资讯"
                        key="资讯"
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' + require('./image/icon_cattle.png') + ') center center /  21px 21px no-repeat'
                            }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' + require('./image/icon_cattle_on.png') + ') center center /  21px 21px no-repeat'
                            }}
                            />
                        }
                        selected={true}
                        onPress={() => { this.props.history.push('/cattle'); }}
                    />
                    <TabBar.Item
                        icon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' + require('./image/icon_put.png') + ') center center /  21px 21px no-repeat'
                            }}
                            />
                        }
                        selectedIcon={
                            <div style={{
                                width: '22px',
                                height: '22px',
                                background: 'url(' + require('./image/icon_put_on.png') + ') center center /  21px 21px no-repeat'
                            }}
                            />
                        }
                        title="发布"
                        key="发布"
                        onPress={() => { this.props.history.push('/edit'); }}
                    />
                </TabBar>
            </div>
        )
    }
}