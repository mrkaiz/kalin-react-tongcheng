import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css';
import Route from './route.jsx';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Route />, document.getElementById('root'));
registerServiceWorker();
