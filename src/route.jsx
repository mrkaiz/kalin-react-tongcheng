import React, { Component } from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import 'antd-mobile/dist/antd-mobile.css';
import { setUser } from "./api";

import List from './List.jsx';
import Detailed from './detailed.jsx';
import Edit from './edit.jsx';
import Input from './input.jsx';
import Login from './login.jsx';
import Cattle from './cattle.jsx';
import Article from './article.jsx';

class App extends Component {
	constructor() {
		super();
		this.state = {
			token: '',
			uid: ''
		}
		this.setUser = this.setUser.bind(this);
	}
	setUser(uid, token) {
		setUser(uid, token);
		this.setState({
			token: token,
			uid: uid
		})
	}
	render() {
		var { token, uid } = this.state;
		if (token && uid > 0) {
			return (
				<BrowserRouter>
					<Switch>
						<Route exact path="/" component={List} />
						<Route exact path="/list" component={List} />
						<Route exact path="/detailed/:id" component={Detailed} />
						<Route exact path="/edit" component={Edit} />
						<Route exact path="/input" component={Input} />
						<Route exact path="/cattle" component={Cattle} />
						<Route exact path="/article/:id" component={Article} />
					</Switch>
				</BrowserRouter>
			);
		} else {
			return (
				<Login setUser={this.setUser} />
			)
		}
	}

}

export default App;
