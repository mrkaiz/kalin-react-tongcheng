import React, { Component } from 'react';
import './style.css';

export default class Notice extends Component {

    render() {
        var { img, text } = this.props;
        return (
            <div className="Notice">
                <div className="icon"><img src={img} alt="" /></div><div className="text"><p>{text}</p></div>
            </div>
        )
    }
}