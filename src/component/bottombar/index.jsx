import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import './style.css';

export default class Bottombar extends Component {
    render() {
        var { menu } = this.props;
        var list = menu.map(function (item, i) {
            return (
                <li key={i}>
                    <NavLink to={item.to}>
                        <img src={item.img} alt="" />
                        <p>{item.text}</p>
                    </NavLink >
                </li>
            )
        })
        return (
            <ul className="Bottombar">
                {list}
            </ul>
        );
    }
}

