export { default as Notice } from './notice/index';
export { default as Search } from './search/index';
export { default as Bottombar } from './bottombar/index';
export { default as Chexun } from './chexun/index';
export { default as Address } from './address/index';
export { default as Slider } from './slider/index';
export { default as News } from './news/index';