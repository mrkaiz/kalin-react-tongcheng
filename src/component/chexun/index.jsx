import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './style.css';

export default class Chexun extends Component {
    afterday(d = 0) {
        var day = 24 * 60 * 60;
        var today = Math.round(new Date(new Date(new Date().toLocaleDateString()).getTime()).getTime() / 1000);
        return today + day * d
    }

    formatDateTime(inputTime) {
        var date = new Date(inputTime * 1000);
        // var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? ('0' + m) : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        var h = date.getHours();
        h = h < 10 ? ('0' + h) : h;
        var minute = date.getMinutes();
        // var second = date.getSeconds();
        minute = minute < 10 ? ('0' + minute) : minute;
        // second = second < 10 ? ('0' + second) : second;
        if (inputTime > this.afterday(-1) && inputTime < this.afterday(0)) {
            return '昨天 ' + h + ':' + minute;
        }
        if (inputTime > this.afterday() && inputTime < this.afterday(1)) {
            return '今天 ' + h + ':' + minute;
        }
        if (inputTime > this.afterday(1) && inputTime < this.afterday(2)) {
            return '明天 ' + h + ':' + minute;
        }
        return m + '-' + d + ' ' + h + ':' + minute;
    };
    formatPhone(inputPhone) {
        return String(inputPhone).replace(/^(\d{4})\d{4}(\d+)/, "$1****$2");
    }
    render() {
        var { data } = this.props;
        var head = data.headimgurl || require('../../image/head.png');
        return (
            <div className="Chexun">
                <Link to={"/detailed/" + data.id}>
                    <div className="title">
                        <div className="head"><img src={head} alt="" /></div>
                        <div className="user">{this.formatPhone(data.mobile)}</div>
                        <div className="time">{this.formatDateTime(data.time)}</div>
                    </div>
                    <div className="border1px"></div>
                    <div className="frist">
                        <i></i><p>{data.start}</p>
                    </div>
                    <div className="last">
                        <i></i><p>{data.end}</p>
                    </div>
                    <div className="more">查看详细</div>
                </Link>
            </div>
        )
    }
}