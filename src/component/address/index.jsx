import React, { Component } from 'react';
import './style.css';


export default class Address extends Component {
    state = {
        list: ['', ''],
    }
    handleData(index, e) {
        var { list } = this.state;
        list[index] = e.target.value;
        this.setState({
            list: list,
        })
        this.props.onChange(list);
    }
    onDelete(index, e) {
        var { list } = this.state;
        if (index === this.state.list.length - 1) {
            this.setState({
                list: [...this.state.list, ''],
            })
        } else {
            list.splice(index, 1)
            this.setState({
                list: list
            })
        }
    }
    render() {
        var { disabled, data } = this.props;
        var list = data || this.state.list;
        return (
            <div className="Address">
                {list.map((item, index) => (
                    <div key={index} className="item" ref="list">
                        <i></i><div className="edit">
                            {
                                !disabled ? <div className="add" onClick={this.onDelete.bind(this, index)}></div> : null
                            }
                            <input type="text" onChange={this.handleData.bind(this, index)} value={item} placeholder="请输入地址" disabled={disabled} /></div>
                    </div>
                ))}
            </div>
        )
    }
}