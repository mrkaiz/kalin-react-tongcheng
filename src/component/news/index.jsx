import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './style.css';

export default class News extends Component {
    render() {
        return (
            <div className="new-list">
                <Link to={'/article/1'}>
                    <div className="top">
                    <div className="pic" style={{ backgroundImage: "url(" + require("../../image/load_error.png") + ")" }}></div>
                        <div className="list-txt">
                            <div className="title">震撼!石牛寨斗牛表演赛高潮迭起,现场座无虚席!震撼!石牛寨</div>
                            <div className="time">
                                <span className="s1">玻璃桥景区石牛寨</span>
                                <span className="s2">47分钟前</span>
                            </div>
                        </div>
                    </div>
                </Link >
            </div >
        );
    }
}

