import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './style.css';

class Search extends Component {
    constructor() {
        super();
        this.state = {
            btn: false,
            start: '',
            end: '',
        }
        this.btnShow = this.btnShow.bind(this);
        this.btnHide = this.btnHide.bind(this);
        this.search = this.search.bind(this);
        this.edit = this.edit.bind(this);
    }
    edit(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    btnShow() {
        this.setState({
            btn: true
        })
    }
    btnHide() {
        this.setState({
            btn: false
        })
    }
    search() {
        this.props.search(this.state.start, this.state.end)
        this.setState({
            start: '',
            end: ''
        })
        this.btnHide()
    }
    render() {
        return (
            <div className="Search">
                <div className="edit" onFocus={this.btnShow} ><i className="start"></i><input className="s" name="start" onChange={this.edit} value={this.state.start} type="text" placeholder="当前定位位置" /></div>
                <div className="edit"><div className="border1px"></div></div>
                <div className="edit" onFocus={this.btnShow} ><i className="end"></i><input name="end" onChange={this.edit} value={this.state.end} className="e" type="text" placeholder="想要去哪儿？" />{this.state.btn ? <div className="btn" onClick={this.search}>搜索</div> : null}</div>
            </div>
        )
    }
}
export default Search = withRouter(Search);