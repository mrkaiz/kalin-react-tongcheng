import React, { Component } from 'react';
import { DatePicker, List } from 'antd-mobile';
import { Address } from './component/component.jsx';
import { InputItem, Toast } from 'antd-mobile';
import { addTravel } from "./api";

import "./css/edit.css";
const nowTimeStamp = Date.now();
const now = new Date(nowTimeStamp);
export default class Detailed extends Component {
    constructor() {
        super();
        this.state = {
            date: now,
            phone: '',
            remarks: '',
            address: '',
        }
    }
    renderContent = tab => (
        <div className="cart-box">
            <div className="cart" style={{ backgroundImage: "url(" + tab.img + ")" }}></div>
        </div>
    );
    //去掉所有空格
    Trim(str) {
        return str.replace(/\s+/g, "");
    }
    save() {
        var { date, phone, remarks, address } = this.state;
        if (!date) {
            Toast.info('时间不能为空');
            return;
        }
        if (!phone) {
            Toast.info('手机号不能为空');
            return;
        }
        if (!address) {
            Toast.info('地址不能为空');
            return;
        }
        var data = {
            start_time: Math.round(date.getTime() / 1000),
            mobile: this.Trim(phone),
            remarks: remarks,
            address: address
        }
        addTravel(data).then(res => {
            if (res.code === '0000') {
                this.props.history.push('/list');
            } else {
                Toast.info('发布失败，请重试');
            }
        })
    }

    render() {
        // const tabs = [
        //     { title: '小轿车', img: require('./image/cart.png') },
        //     { title: '越野车', img: require('./image/cart.png') },
        //     { title: '面包车', img: require('./image/cart.png') },
        // ];
        return (
            <div className="Edit">
                {/* <Tabs tabs={tabs} onChange={(cart) => { this.setState({ cart: cart }) }} >
                    {this.renderContent}
                </Tabs> */}
                <DatePicker value={this.state.date} onChange={date => this.setState({ date })}>
                    <List.Item arrow="horizontal">出发时间</List.Item>
                </DatePicker>
                <InputItem
                    onChange={(phone) => { this.setState({ phone: phone }) }}
                    type="phone"
                    placeholder="000 0000 0000"
                >手机号码</InputItem>
                <Address disabled={false} onChange={(e) => this.setState({ address: e })} />
                <div className="remarks"><textarea onChange={(e) => this.setState({ remarks: e.target.value })} cols="30" rows="10" placeholder="备注"></textarea></div>
                <div className="btn-box">
                    <div className="cancel"><div className="btn" onClick={() => this.props.history.push('/list')}>取消</div> </div>
                    <div className="submit"><div className="btn" onClick={this.save.bind(this)}>确定</div></div>
                </div>
            </div >
        )
    }
}