import React, { Component } from 'react';
import { Map } from 'react-amap';
import './css/input.css';

export default class Input extends Component {
    constructor() {
        super();
        this.state = {
            lng: '',
            lat: ''
        }
        const self = this;
        this.toolEvents = {
            created: (tool) => {
                console.log(tool)
                self.tool = tool;
            },
            draw({ obj }) {
                self.drawWhat(obj);
            }
        }
        this.mapPlugins = [{
            name: 'ToolBar',
            options: {
                visible: true,
                position: 'RB',
                autoPosition: true,
                features:"road"
            }
        }];
        this.mapCenter = { longitude: this.state.lng, latitude: this.state.lat };
        this.events = {
            click: (e) => {
                console.log(e.lnglat.lng, e.lnglat.lat)
            },
        }
    }
    render() {
        return (
            <div className="Map">
                <div className="edit-box">
                    <div className="edit-item">
                        <div className="item">
                            <i></i>
                            <div className="edit">
                                <input type="text" placeholder="请输入地址" />
                            </div>
                        </div>
                        <div className="item">
                            <i></i>
                            <div className="edit">
                                <input type="text" placeholder="请输入地址" />
                            </div>
                        </div>
                    </div>
                </div>
                <Map
                    amapkey="fb70f036b1c3c6474223cefbb9c1b509"
                    plugins={this.mapPlugins}
                    zoom={14}
                    center={this.state.lng ? this.mapCenter : null}
                    events={this.events}
                >
                    <ZoomCtrl />
                </Map>
                <div className="btn-box"><div className="btn">确认搜索</div></div>
            </div >
        )
    }
}
const ZoomCtrl = (props) => {
    const map = props.__map__;
    if (!map) {
        return;
    }
    const zoomIn = () => map.zoomIn();
    const zoomOut = () => map.zoomOut();
    console.log(map);

    return (
        <div className="zoomcontrol">
            <div className="zoom-touch-plus">
                <div onClick={zoomIn}>+</div>
            </div>
            <div className="zoom-touch-plus">
                <div onClick={zoomOut}>−</div>
            </div>
        </div>
    );
};