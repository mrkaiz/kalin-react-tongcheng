import React, { Component } from 'react';
import { Notice, Address } from './component/component.jsx';
import { getTravelDetailed } from "./api";

import "./css/detailed.css";
let cachedata;
export default class Detailed extends Component {
    constructor() {
        super();
        this.state = {
            data: {

            },
            id: 0,
        }
    }
    componentDidMount() {
        var id = this.props.match.params.id;
        if (cachedata) {
            if (cachedata.id !== id) {
                this.getDetailed(id)
            } else {
                this.setState(cachedata);
            }
        } else {
            this.getDetailed(id)
        }

    }
    getDetailed(id) {
        getTravelDetailed(id).then(res => {
            this.setState({
                data: res.data,
                id: id
            })
        })
        window.scrollTo(0, 0);
    }
    componentWillUnmount() {
        cachedata = this.state;
    }
    formatDateTime(inputTime) {
        if (!inputTime) {
            return "0000-00-00 00:00";
        }
        var date = new Date(inputTime * 1000);
        var y = date.getFullYear();
        var m = date.getMonth() + 1;
        m = m < 10 ? ('0' + m) : m;
        var d = date.getDate();
        d = d < 10 ? ('0' + d) : d;
        var h = date.getHours();
        h = h < 10 ? ('0' + h) : h;
        var minute = date.getMinutes();
        minute = minute < 10 ? ('0' + minute) : minute;
        return y + '-' + m + '-' + d + ' ' + h + ':' + minute;
    };
    render() {
        var { data } = this.state;
        return (
            <div className="Detailed">
                <div className="cart" style={{ backgroundImage: "url(" + require('./image/cart.png') + ")" }}></div>
                <Notice img={require('./image/icon_time.png')} text={"出发时间：" + this.formatDateTime(data.time)} />
                <Address disabled={true} data={data.route} />
                <div className="remarks">{data.remarks}</div>
                <a href={"tel:" + data.mobile} className="call">拨打电话</a>
            </div>
        )
    }
}