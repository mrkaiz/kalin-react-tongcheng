import React, { Component } from 'react';
import wx from 'weixin-js-sdk';
import axios from 'axios';
import './css/article.css';

export default class Article extends Component {

    componentDidMount() {
        var redirectURL = window.location.href;
        var _url = "http://anliantu.com/index/login/getJsSign?URL=" + redirectURL;
        var that = this;
        axios.get(_url).then(function (data) {
            that.wechat(data.data)
        })

    }
    wechat(data) {
        wx.config({
            debug: false,
            appId: data.appId,
            timestamp: data.timestamp,
            nonceStr: data.nonceStr,
            signature: data.signature,
            jsApiList: [
                'checkJsApi',
                'onMenuShareAppMessage',
                'onMenuShareTimeline',
                'onMenuShareQQ',
                'onMenuShareQZone',
                'onMenuShareWeibo',
            ]
        });
        var share_config = {
            "share": {
                "imgUrl": "http:/huaxiaoujia.com/logo.png", //分享图，默认当相对路径处理，所以使用绝对路径的的话，“http://”协议前缀必须在。
                "title": "点击拨打电话", //摘要,如果分享到朋友圈的话，不显示摘要。
                "desc": "分享卡片标题", //分享卡片标题
                "link": window.location.href, //分享出去后的链接，这里可以将链接设置为另一个页面。
                "success": function () { //分享成功后的回调函数
                },
                'cancel': function () {
                    // 用户取消分享后执行的回调函数
                }
            }
        };
        wx.ready(function () {
            wx.checkJsApi({
                jsApiList: [
                    'getLocation'
                ],
                success: function (res) {
                    if (res.checkResult.getLocation === false) {
                        alert('你的微信版本太低，不支持微信JS接口，请升级到最新的微信版本！');
                        return;
                    }
                }
            });
            wx.onMenuShareAppMessage(share_config.share); //分享给好友
            wx.onMenuShareQQ(share_config.share); //分享给手机QQ
            wx.onMenuShareQZone(share_config.share) //分享QQ空间
            wx.onMenuShareWeibo(share_config.share) //分享到腾讯微博
            wx.onMenuShareTimeline({
                title: "分享标题", // 分享标题
                link: window.location.href, // 分享链接
                imgUrl: "http:/huaxiaoujia.com/logo.png", // 分享图标
                success: function () {
                    // 用户确认分享后执行的回调函数
                },
                cancel: function () {
                    // 用户取消分享后执行的回调函数
                }
            });
        })
    }
    render() {
        return (
            <div className="article">
                <div className="title">斗牛节到底有多疯狂？春节不来凯里斗牛城定后悔！</div>
                <div className="time">2018-2-7 咔琳同城</div>
                <div className="content">

                </div>
            </div>
        );
    }
}
